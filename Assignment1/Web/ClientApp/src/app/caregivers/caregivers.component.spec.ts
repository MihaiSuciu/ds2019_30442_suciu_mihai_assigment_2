﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { CaregiversComponent } from './caregivers.component';

let component: CaregiversComponent;
let fixture: ComponentFixture<CaregiversComponent>;

describe('caregivers component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CaregiversComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(CaregiversComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});