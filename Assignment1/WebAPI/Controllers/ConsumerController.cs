﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class ConsumerController : ApiController
    {
        // GET: api/Consumer
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Consumer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Consumer
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Consumer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Consumer/5
        public void Delete(int id)
        {
        }
    }
}
