﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class Activity
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ActivityLabel { get; set; }

        public Activity(string json)
        {
            JObject jObject = JObject.Parse(json);
            StartTime = (DateTime)jObject["StartTime"];
            EndTime = (DateTime)jObject["EndTime"];
            ActivityLabel = (string)jObject["ActivityLabel"];
        }

    }
}