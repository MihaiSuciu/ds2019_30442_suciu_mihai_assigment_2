﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Producer
{
    class Activity
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ActivityLabel { get; set; }
    }
}
