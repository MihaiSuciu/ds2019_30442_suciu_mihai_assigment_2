import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'

import {PatientService} from './shared/patient.service'
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

  constructor(private patientService:PatientService) { }

  ngOnInit() {
  }

}
