import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'

import { MedicationService } from '../shared/medication.service';
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})
export class MedicationComponent implements OnInit {

  constructor(private medicationService:MedicationService,private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null){
      form.reset();
    }
    this.medicationService.selectedMedication={
      MedicationID:0,
      Name:'',
      Dosage:0,
      SideEffects:''
    }
  }
  onSubmit(form:NgForm){
    if(!form.value.MedicationID){
      this.medicationService.postMedication(form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.medicationService.getMedicationList();
          this.toastr.success('New record Added Successfully!','Medication Register');
        })
    }
    else{
      this.medicationService.putMedication(form.value.MedicationID,form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.medicationService.getMedicationList();
          this.toastr.info("Record Updated Successfully!","Medication Register");
        })
    }
  }

}
