import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import{Medication} from './medication.model'
@Injectable({
  providedIn: 'root'
})
export class MedicationService {
  selectedMedication:Medication;
  medicationList:Medication[];

  constructor(private http : Http) { }

  postMedication(medication : Medication){
    var body = JSON.stringify(medication);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post('http://localhost:44341/api/Medication',body,requestOptions).map(x => x.json());
  }
  putMedication(id, medication) {
    var body = JSON.stringify(medication);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:44341/api/Medication/' + id,
      body,
      requestOptions).map(res => res.json());
  }
  getMedicationList(){
    this.http.get('http://localhost:44341/api/Medication')
    .map((data : Response) =>{
      return data.json() as Medication[];
    }).toPromise().then(x => {
      this.medicationList = x;
    })
  }
  deleteMedication(id: number) {
    return this.http.delete('http://localhost:44341/api/Medication/' + id).map(res => res.json());
  }
}
